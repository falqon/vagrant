#vagrant

Requires the vagrant package inifile:

    vagrant plugin install inifile

Running the above command inside the folder with the Vagrantfile will return an error.

# Folder structure

- vagrant: this project
- go:      shared folder
- udok:    project git repo

# Setup shared folders

    cmd /c mklink /D /J .\go\src\github.com\udok-app\udok .\udok\