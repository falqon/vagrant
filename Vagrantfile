# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'inifile'

VAGRANTFILE_API_VERSION = "2"

class VagrantPlugins::ProviderVirtualBox::Action::Network
  def dhcp_server_matches_config?(dhcp_server, config)
    true
  end
end

############### CONSTANTS
CONFIGFILE = ENV['VAGRANT_CONFIG'] || './config/global.ini'

############### Load ini configuration
@settings = IniFile.load(CONFIGFILE)

############### Configure vagrant

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.network :public_network, ip: "192.168.0.45"

  config.vm.box = "kaorimatz/ubuntu-16.04-amd64"

  config.vm.box_url = "kaorimatz/ubuntu-16.04-amd64"

  config.vm.synced_folder @settings['folder']['database'], "/home/vagrant/database", owner: "vagrant", group: "vagrant", mount_options: ["dmode=777,fmode=666"]
  config.vm.synced_folder @settings['folder']['gopath'], "/home/vagrant/go", owner: "vagrant", group: "vagrant", mount_options: ["dmode=777,fmode=666"]
  config.vm.provision :file, source: @settings['folder']['envs'], destination: '/home/vagrant/envs'

  config.vm.provider :virtualbox do |vb|
    vb.name = @settings['project']['name']
  end

  $script = <<-EOT
    # install git
    sudo apt-get install git
    
    # install go
    sudo bash /vagrant/scripts/inst_go.sh --64

    # install postgresql
    sudo bash /vagrant/scripts/install_postgres.sh #{@settings['project']['name']} #{@settings['db']['password']} #{@settings['db']['password']}

    # insert database schema
    sudo -u postgres psql "dbname='#{@settings['project']['name']}'" -f /home/vagrant/database/schema.sql

  EOT

  config.vm.provision :shell, inline: $script, privileged: false

end
