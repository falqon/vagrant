#!/bin/bash

###########################################################
# Script to set up a PostgreSQL project on Vagrant.
###########################################################

# Installation settings
PROJECT_NAME=${1:-db}

# Edit the following to change the name of the database user that will be created (defaults to the project name)
APP_DB_USER=$PROJECT_NAME
APP_DB_PASS=${2:-db}
POSTGRES_DB_PASS=${3:-123}

# Edit the following to change the name of the database that is created (defaults to the project name)
APP_DB_NAME=$PROJECT_NAME

# Edit the following to change the version of PostgreSQL that is installed
PG_VERSION=9.6

###########################################################
# Build dependencies
###########################################################

sudo apt-get install -y curl binutils bison gcc build-essential

###########################################################
# PostgreSQL
###########################################################

print_db_usage () {
  echo "Your PostgreSQL database has been setup and can be accessed on your local machine on the forwarded port (default: 15432)"
  echo "  Host: localhost"
  echo "  Port: 15432"
  echo "  Database: $APP_DB_NAME"
  echo "  Username: $APP_DB_USER"
  echo "  Password: $APP_DB_PASS"
  echo ""
  echo "Admin access to postgres user via VM:"
  echo "  vagrant ssh"
  echo "  sudo su - postgres"
  echo ""
  echo "psql access to app database user via VM:"
  echo "  vagrant ssh"
  echo "  sudo su - postgres"
  echo "  PGUSER=$APP_DB_USER PGPASSWORD=$APP_DB_PASS psql -h localhost $APP_DB_NAME"
  echo ""
  echo "Env variable for application development:"
  echo "  DATABASE_URL=postgresql://$APP_DB_USER:$APP_DB_PASS@localhost:15432/$APP_DB_NAME"
  echo ""
  echo "Local command to access the database via psql:"
  echo "  PGUSER=$APP_DB_USER PGPASSWORD=$APP_DB_PASS psql -h localhost -p 15432 $APP_DB_NAME"
}

export DEBIAN_FRONTEND=noninteractive

PROVISIONED_ON=/etc/vm_provision_on_timestamp
if [ -f "$PROVISIONED_ON" ]
then
  echo "VM was already provisioned at: $(cat $PROVISIONED_ON)"
  echo "To run system updates manually login via 'vagrant ssh' and run 'sudo apt-get update && sudo apt-get upgrade'"
  echo ""
  print_db_usage
  exit
fi

CD=$(lsb_release --codename | grep Codename)
CODENAME=${CD##*:}

PG_REPO_APT_SOURCE=/etc/apt/sources.list.d/pgdg.list
if [ ! -f "$PG_REPO_APT_SOURCE" ]
then
  # Add PG apt repo:
  sudo bash -c "echo \"deb http://apt.postgresql.org/pub/repos/apt/ $CODENAME-pgdg main\" > \"$PG_REPO_APT_SOURCE\""

  # Add PGDG repo key:
  wget --quiet -O - https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
fi

# Update package list and upgrade all packages
sudo apt-get update

### fix grub error
#sudo apt-get purge grub-pc grub-common
#sudo rm -r /etc/grub.d/
#sudo apt-get install grub-pc grub-common
#sudo grub-install /dev/sda
#sudo update-grub
###

DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"

sudo apt-get -y install "postgresql-$PG_VERSION" "postgresql-contrib-$PG_VERSION"

PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

# Edit postgresql.conf to change listen address to '*':
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

# Append to pg_hba.conf to add password auth:
sudo bash -c "echo \"host    all             all             all                     md5\" >> \"$PG_HBA\""

# Explicitly set default client_encoding
sudo bash -c "echo \"client_encoding = utf8\" >> \"$PG_CONF\""

# Restart so that all new config is loaded:
sudo service postgresql restart

sudo echo -e "$POSTGRES_DB_PASS\n$POSTGRES_DB_PASS\n" | sudo passwd postgres

sudo -u postgres psql -c "CREATE USER $APP_DB_USER WITH PASSWORD '$APP_DB_PASS';"
sudo -u postgres psql -c "ALTER USER postgres PASSWORD '$POSTGRES_DB_PASS';"

sudo -u postgres psql -c "CREATE DATABASE $APP_DB_NAME WITH OWNER=$APP_DB_USER LC_COLLATE='en_US.utf8' LC_CTYPE='en_US.utf8' ENCODING='UTF8' TEMPLATE=template0;"


# Tag the provision time:
sudo bash -c "date > \"$PROVISIONED_ON\""

echo "Successfully created virtual machine."
echo ""
print_db_usage
